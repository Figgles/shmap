#include <stdio.h>
#include <unistd.h>
#include <libgen.h>
#include <stdint.h>

#define STB_IMAGE_IMPLEMENTATION
#include "./ext/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "./ext/stb_image_write.h"

typedef struct Color {
  uint8_t r, g, b, a;
} Color;

Color color_from_u32(uint32_t color) {
  return (Color) {
    .r = (color >> (8 * 2)) & 0xFF,
    .g = (color >> (8 * 1)) & 0xFF,
    .b = (color >> (8 * 0)) & 0xFF,
    .a = 255,
  };
}

Color color_lerp(Color a, Color b, float t) {
  return (Color) {
    .r = a.r + (b.r - a.r) * t,
    .g = a.g + (b.g - a.g) * t,
    .b = a.b + (b.b - a.b) * t,
    .a = 255,
  };
}

typedef struct Gray {
  uint8_t v, a;
} Gray;

void usage() {
  fprintf(stderr, "USAGE: shmap <filename> <bg-color> <fg-color>\n");
}

int main(int argc, char **argv) {
  if (argc != 4) {
    usage();
    return EXIT_FAILURE;
  }

  if (access(argv[1], F_OK) != 0) {
    fprintf(stderr, "ERROR: File %s does not exist\n", argv[1]);
    return EXIT_FAILURE;
  }

  if (stbi_info(argv[1], NULL, NULL, NULL) == 0) {
    fprintf(stderr, "ERROR: Filetype not supported by stbi\n");
    return EXIT_FAILURE;
  }

  Color color_table[256] = {};
  uint32_t bg_packed = strtol(argv[2], NULL, 16);
  Color bg = color_from_u32(bg_packed);
  uint32_t fg_packed = strtol(argv[3], NULL, 16);
  Color fg = color_from_u32(fg_packed);
  for (int i = 0; i < 256; i ++) {
    color_table[i] = color_lerp(bg, fg, ((float)i)/256.f);
  }
  
  int x, y, n;
  Gray *pixels = (void*)stbi_load(argv[1], &x, &y, &n, 2);
  Color *result = malloc(x * y * sizeof(Color));

  for (int i = 0; i < x * y; i++) {
    result[i] = color_table[pixels[i].v];
    result[i].a = pixels[i].a;
  }

  const char *new_file_prefix = "./new-";
  char *new_filename = malloc(strlen(argv[1]) + strlen(new_file_prefix));
  strcpy(new_filename, new_file_prefix);
  strcat(new_filename, basename(argv[1]));

  stbi_write_png(new_filename, x, y, 4, result, 4*x);
  
  printf("%s\n", new_filename);
  
  free(new_filename);
  stbi_image_free(pixels);
  return EXIT_SUCCESS;
}
