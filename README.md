# shmap: a quick and easy image recolor tool to match your favourite theme

## Usage

### compile:
`$ make`

### install:
`$ sudo make install`

### run:
`$ shmap <filename> <bg-color> <fg-color>`

After running, the new image will be written to `new-<image-path>` in the current working directory

## Examples
Using `shmap tux.png 303446 c6d0f5`, we can turn this:

![Tux](/examples/tux.png)

into this:

![Recolored Tux](/examples/new-tux.png)

## TODO
* [DONE] blend with original image, given lerp value