debug: src/main.c
	$(CC) -o shmap src/main.c \
	-lm -ggdb \
	-Wextra -Wall

release: src/main.c
	$(CC) -o shmap src/main.c \
	-lm -O2 \
	-Wextra -Wall

install:
	cp ./shmap /usr/bin/shmap
